import React, { Component } from 'react';
import MapControls from './map-controls.js';

export default class Map extends Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    return (
      <div>
        <MapControls />
        <p>Map goes here!</p>
      </div>
    )
  }
}